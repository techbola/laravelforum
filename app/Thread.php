<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    use CommentableTrait, RecordsFeed;

    protected $fillable=['subject', 'type', 'thread', 'user_id'];

    public static function boot()
    {
        parent::boot();

//        static::created(function($thread){
//            $thread->recordFeed('created');
//        });

//        static::created(function($thread){
//            $thread->recordFeed('deleted');
//        });
    }

//    protected function recordFeed($event)
//    {
//        $this->feeds()->create([
//            'user_id' => auth()->id(),
//            'type' => $event.'_'.strtolower(class_basename($this)) //created_thread
//        ]);
//    }
//
//    public function feeds()
//    {
//        return $this->morphMany(Feed::class, 'feedable');
//    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

}
