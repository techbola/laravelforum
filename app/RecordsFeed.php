<?php
/**
 * Created by PhpStorm.
 * User: techbola
 * Date: 7/25/2018
 * Time: 12:57 PM
 */

namespace App;


trait RecordsFeed
{

    protected static function bootRecordsFeed()
    {
        static::created(function($thread){
            $thread->recordFeed('created');
        });
    }

    protected function recordFeed($event)
    {
        $this->feeds()->create([
            'user_id' => auth()->id(),
            'type' => $event.'_'.strtolower(class_basename($this)) //created_thread
        ]);
    }

    public function feeds()
    {
        return $this->morphMany(Feed::class, 'feedable');
    }

}