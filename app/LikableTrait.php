<?php
/**
 * Created by PhpStorm.
 * User: techbola
 * Date: 7/16/2018
 * Time: 2:50 PM
 */

namespace App;


trait LikableTrait
{

    public function likes()
    {
        return $this->morphMany(Like::class, 'likable');
    }

    public function likeIt()
    {
        $like = new Like();
        $like->user_id = auth()->user()->id;

        $this->likes()->save($like);

        return $like;
    }

    public function unlikeIt()
    {
//        $like = Like::find($id);
//        $like->delete();
//
//        return true;

        $this->likes()->where('user_id',auth()->id())->delete();
    }

    public function isLiked()
    {
        return !!$this->likes()->where('user_id', auth()->id())->count();
    }


}