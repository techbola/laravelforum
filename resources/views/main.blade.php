@extends('layouts.front')

@section('content')

    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Join Our Forum Community</h1>
            <p class="lead">Help and get help</p>
            <p class="btn btn-primary btn-lg">Learn More</p>
        </div>
    </div>

@endsection