@extends('layouts.front')

@section('user_profile')

    <div class="dp">
        <img src="https://dummyimage.com/150" alt="">
    </div>
    <h3>
        {{ auth()->user()->name }}
    </h3>

    <br>

@endsection

@section('content')

    <div>

        <h3>Activity Feeds</h3>

        @foreach($feeds as $feed)

            @include("feeds.$feed->type")

        @endforeach

        {{--<h3>{{ $user->name }}'s latest Threads</h3>--}}

        {{--@forelse($threads as $thread)--}}

            {{--<h5>{{ $thread->subject }}</h5>--}}

        {{--@empty--}}

            {{--<h5>No threads yet</h5>--}}

        {{--@endforelse--}}

        {{--<br><br>--}}

        {{--<h3>{{ $user->name }}'s latest Comments</h3>--}}

        {{--@forelse($comments as $comment)--}}

            {{--<h5>--}}
                {{--{{ $user->name }} commented on--}}
                {{--<a href="{{ route('thread.show',$comment->commentable->id) }}">--}}
                    {{--{{ $comment->commentable->subject }}--}}
                {{--</a>--}}
                {{--{{ $comment->created_at->diffForHumans() }}--}}
            {{--</h5>--}}

        {{--@empty--}}

            {{--<h5>No comments yet</h5>--}}

        {{--@endforelse--}}

    </div>

@endsection