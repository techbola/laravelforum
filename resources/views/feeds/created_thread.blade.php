<div class="card card-primary">
    <div class="card-body">
        <div class="card-title">
            <h3>{{ $feed->type }}</h3>
        </div>
        <div class="card-subtitle">
            <p>
                User created {{ $feed->feedable->thread }}
            </p>
        </div>
    </div>
</div>