@extends('layouts.front')

@section('heading')
    Create Thread
@endsection

@section('content')

    @include('layouts.includes.error')

    @include('layouts.includes.success')

    <form action="{{ route('thread.update', $thread->id) }}" method="post" role="form" style="padding-bottom: 20px;">

        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <div class="form-group">
            <label for="subject">Subject</label>
            <input type="text" class="form-control" name="subject" id="subject" value="{{ $thread->subject }}" placeholder="Input...">
        </div>

        <div class="form-group">
            <label for="type">Type</label>
            <input type="text" class="form-control" name="type" id="type" value="{{ $thread->type }}" placeholder="Input...">
        </div>

        <div class="form-group">
            <label for="thread">Thread</label>
            <textarea name="thread" id="thread" class="form-control" cols="30" rows="10">{{ $thread->thread }}</textarea>
        </div>

        <button type="submit" class="btn btn-primary">Update</button>
    </form>

@endsection