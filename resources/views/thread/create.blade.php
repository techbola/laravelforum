@extends('layouts.front')

@section('heading')
    Create Thread
@endsection

@section('content')

    @include('layouts.includes.error')

    @include('layouts.includes.success')

    <form action="{{ route('thread.store') }}" method="post" role="form" style="padding-bottom: 20px;">

        {{ csrf_field() }}

        <div class="form-group">
            <label for="subject">Subject</label>
            <input type="text" class="form-control" name="subject" id="subject" value="{{ old('subject') }}" placeholder="Input...">
        </div>

        <div class="form-group">
            <label for="tag">Tags</label>
            <select class="form-control" name="tags[]" id="tag" multiple>

                @foreach($tags as $tag)

                    <option value="{{ $tag->id }}">{{ $tag->name }}</option>

                @endforeach

            </select>
        </div>

        <div class="form-group">
            <label for="thread">Thread</label>
            <textarea name="thread" id="thread" class="form-control" cols="30" rows="5">{{ old('thread') }}</textarea>
        </div>

        {{--<div class="form-group">--}}
            {{--{!! NoCaptcha::renderJs() !!}--}}
            {{--{!! NoCaptcha::display() !!}--}}
        {{--</div>--}}

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection

@section('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>

    <script>
        $(function () {

            $('#tag').selectize();

        })
    </script>

@endsection