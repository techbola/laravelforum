@extends('layouts.front')

@section('heading')
    Threads
@endsection

@section('content')

    @include('thread.includes.thread-list')

@endsection