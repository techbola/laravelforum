<div class="list-group">
    @forelse($threads as $thread)

        <div class="card card-primary">
            <div class="card-body">
                <div class="card-title">
                    <a href="{{ route('thread.show', $thread->id) }}">
                        {{ $thread->subject }}
                    </a>
                </div>
                <div class="card-subtitle">
                    <p>
                        {{ str_limit($thread->thread, 100) }}
                        <br>
                        Posted by
                        <a href="{{ route('user_profile', $thread->user->name) }} ">
                            {{ $thread->user->name }}
                        </a>
                        {{$thread->created_at->diffForHumans()}}
                    </p>
                </div>
            </div>
        </div>

    @empty
        <h5>No threads</h5>

    @endforelse
</div>