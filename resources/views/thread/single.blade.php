@extends('layouts.front')

@section('heading')

    @include('layouts.includes.error')

    @include('layouts.includes.success')

    {{ $thread->subject }}

@endsection

@section('content')

    <div class="thread-details">
        {!! \Michelf\Markdown::defaultTransform($thread->thread) !!}
    </div>

    <br>

    @if(auth()->check())

        @if(auth()->user()->id == $thread->user_id)

            <div class="actions">

                <a href="{{ route('thread.edit', $thread->id) }}" class="btn btn-info btn-sm">Edit</a>

                <form action="{{ route('thread.destroy', $thread->id) }}" method="POST" role="form" class="delete-form">

                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    <input type="submit" class="btn btn-sm btn-danger" value="Delete">

                </form>

            </div>

            <br><br>

        @endif

    @endif

    {{--Answer/Comment--}}

    <h4>Comments</h4>

    @foreach($thread->comments as $comment)

        <div class="comment-list">

        <h4>{{ $comment->body }}</h4>

        <lead>{{ $comment->user->name }}</lead>

        @if(auth()->user()->id == $comment->user_id)

            <div class="actions">

                <button class="btn btn-sm btn-default" id="{{ $comment->id }}-count">
                    {{ $comment->likes()->count() }}
                </button>
                <span class="btn btn-sm btn-default {{ $comment->isLiked() ? 'liked' : '' }}" onclick="likeIt('{{ $comment->id }}', this)">
                    <span class="fa fa-heart"></span>
                </span>

                <a class="btn btn-primary btn-sm" data-toggle="modal" href="#{{ $comment->id }}">Edit</a>
                <div class="modal fade" id="{{ $comment->id }}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                                <h4 class="modal-title">Edit Comment</h4>
                            </div>
                            <div class="modal-body">

                                <div class="comment-form">
                                    <form action="{{ route('comment.update', $comment->id) }}" method="post" role="form">

                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}

                                        <div class="form-group">
                                            <input type="text" class="form-control" name="body" id="body" value="{{ $comment->body }}">
                                        </div>

                                        <button type="submit" class="btn btn-primary">Comment</button>
                                    </form>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <form action="{{ route('comment.destroy', $comment->id) }}" method="POST" role="form" class="delete-form">

                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    <input type="submit" class="btn btn-sm btn-danger" value="Delete">

                </form>

            </div>

        </div>

        @endif

        <br><br>

        <button class="btn btn-sm btn-default float-left" onclick="toggleReply('{{ $comment->id }}')">
            Reply
        </button>

        @if(!empty($thread->solution))

            @if($thread->solution == $comment->id)
                <button class="btn btn-primary btn-sm float-right">Solution</button>
            @endif

        @else

            {{--@if(auth()->check())--}}

                {{--@if($thread->user_id == auth()->user()->id)--}}

                    {{--<form action="{{ route('markAsSolution') }}" method="post">--}}
                        {{--{{ csrf_field() }}--}}
                        {{--<input type="hidden" name="threadId" value="{{ $thread->id }}">--}}
                        {{--<input type="hidden" name="solutionId" value="{{ $comment->id }}">--}}
                        {{--<input type="submit" class="btn btn-success btn-sm float-right" name="{{ $comment->id }}" id="{{ $comment->id }}" value="Mark as solution">--}}
                    {{--</form>--}}

                    @can('update',$thread)
                        <div id="markItSolution" class="btn btn-success float-right" onclick="markAsSolution('{{ $thread->id }}', '{{ $comment->id }}', this)">
                            Mark as solution
                        </div>
                    @endcan

                {{--@endif--}}

            {{--@endif--}}

        @endif

        <br>
        <hr>

        {{--reply form--}}

        <div class="reply-form-{{ $comment->id }} d-none">
            <form action="{{ route('replycomment.store', $comment->id) }}" method="post" role="form">

                {{ csrf_field() }}

                <legend>Reply</legend>

                <div class="form-group">
                    <input type="text" class="form-control" name="body" id="body" placeholder="Reply...">
                </div>

                <button type="submit" class="btn btn-primary">Reply</button>
            </form>
        </div>

        {{--reply to comment--}}

        @if($comment->comments->count() > 0)

            <h6>Replies to comment {{ $comment->id }}</h6>

            @foreach($comment->comments as $reply)

                <div class="small well text-info reply-list" style="margin-left:40px">
                    <p style="font-size: 15px;">{{ $reply->body }}</p>
                    <lead> by {{ $reply->user->name }}</lead>

                    <br>

                    <div class="actions">

                        <a class="btn btn-primary btn-sm" data-toggle="modal" href="#{{ $reply->id }}">Edit</a>
                        <div class="modal fade" id="{{ $reply->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                        </button>
                                        <h4 class="modal-title">Edit Reply</h4>
                                    </div>
                                    <div class="modal-body">

                                        <div class="comment-form">
                                            <form action="" method="post" role="form">

                                                {{ csrf_field() }}
                                                {{ method_field('PUT') }}

                                                <div class="form-group">
                                                    <input type="text" class="form-control" name="body" id="body" value="{{ $comment->body }}">
                                                </div>

                                                <button type="submit" class="btn btn-primary">Reply</button>
                                            </form>
                                        </div>

                                    </div>

                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->

                        <form action="" method="POST" role="form" class="delete-form">

                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <input type="submit" class="btn btn-sm btn-danger" value="Delete">

                        </form>

                    </div>

                </div>

                <br>

                <hr>

            @endforeach

        @endif

    @endforeach

    
    <div class="comment-form">
        <form action="{{ route('threadcomment.store', $thread->id) }}" method="post" role="form">

            {{ csrf_field() }}

            <legend>Comment</legend>

            <div class="form-group">
                <input type="text" class="form-control" name="body" id="body" placeholder="Input...">
            </div>

            <button type="submit" class="btn btn-primary">Comment</button>
        </form>
    </div>

@endsection

@section('js')

    <script>

        function toggleReply(commentId) {
            $('.reply-form-' +commentId).toggleClass('d-none');
        }

        function markAsSolution(threadId, solutionId, elem) {
            var csrfToken = '{{ csrf_token() }}';
            $.post('{{ route('markAsSolution') }}', {solutionId: solutionId, threadId: threadId, _token: csrfToken}, function (data) {
                // console.log(data);
                $(elem).text('Solution');
            });

        }

        function likeIt(commentId, elem) {
            var csrfToken = '{{ csrf_token() }}';
            var likesCount = parseInt($('#'+commentId+"-count").text());
            $.post('{{ route('toggleLike') }}', {commentId: commentId, _token: csrfToken}, function (data) {
                console.log(data);
                // $(elem).css({color:'red'});
                if (data.message === 'liked'){
                    $(elem).addClass('liked');
                    $('#'+commentId+"-count").text(likesCount+1);
                }
                else{
                    $('#'+commentId+"-count").text(likesCount-1);
                    $(elem).removeClass('liked');
                }
            });

        }

    </script>

@endsection