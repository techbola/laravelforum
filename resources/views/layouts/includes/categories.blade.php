<div class="col-md-3">

    @yield('user_profile')

    <ul class="list-group">
        <a href="{{ route('thread.index') }}" class="list-group-item">
            <span class="badge">14</span>
            All Thread
        </a>

        @foreach($tags as $tag)

            <a href="{{ route('thread.index', ['tags'=>$tag->id]) }}" class="list-group-item">
                <span class="badge">2</span>
                {{ $tag->name }}
            </a>

        @endforeach

    </ul>

</div>