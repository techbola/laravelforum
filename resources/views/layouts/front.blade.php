<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Forum Website</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

</head>
<body>

@include('layouts.includes.navbar')

@yield('banner')

    <div class="container" style="padding-top: 20px;">

        <div class="row">
            <div class="col-md-3">

            </div>
            <div class="col-md-9" style="padding-bottom: 20px;">
                <div class="row">
                    <div class="col-md-4">
                        <h4 class="main-content-heading">
                            @yield('heading')
                        </h4>
                    </div>
                    <div class="col-md-2 offset-6">
                        <a href="{{ route('thread.create') }}" class="btn btn-primary">Create Thread</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            {{--category section--}}
            @include('layouts.includes.categories')

            <div class="col-md-9">
                <div class="content-wrap well">
                    @yield('content')
                </div>
            </div>

        </div>

    </div>

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="{{ asset('js/main.js') }}"></script>

@yield('js')

</body>
</html>